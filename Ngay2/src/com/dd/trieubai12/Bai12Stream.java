package com.dd.trieubai12;

import com.dd.trieu.product1.Product;

import java.util.List;
import java.util.stream.Collectors;

public class Bai12Stream {
    public static List<Product> filterProductByQuility(List<Product> productList, boolean isDelete) {

        List<Product> products =  productList.stream().filter(product -> product.isDelete() == false && product.getQulity() > 0).collect(Collectors.toList());
        return  products;
    }
}
