package com.dd.trieubai12;

import com.dd.trieu.product1.Product;

import java.text.ParseException;
import java.util.List;

public class Bai12Demo {
    public static void main(String[] args) throws ParseException {
        List<Product> productList = Product.getPRoduct();

        System.out.println("dùng Stream");
        List<Product> product = Bai12Stream.filterProductByQuility(productList, true);
        System.out.println("Stream : /d" +product);

        System.out.println("Không dùng Stream");
        List<Product> product1 = Bai12.filterProductByQuility(productList, true);
        System.out.println("Stream : /d" +product);


    }
}
