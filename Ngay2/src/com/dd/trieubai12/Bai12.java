package com.dd.trieubai12;

import com.dd.trieu.product1.Product;

import java.util.ArrayList;
import java.util.List;

public class Bai12 {
    public static List<Product> filterProductByQuility(List<Product> productList, boolean isDelete) {
        List<Product> products = new ArrayList<Product>();
        for (Product product : productList) {
            if (product.getQulity() > 0 && isDelete == product.isDelete()) {
                products.add(product);
            }
        }
        return products;

    }
}
