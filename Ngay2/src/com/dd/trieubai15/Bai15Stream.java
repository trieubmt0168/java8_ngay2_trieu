package com.dd.trieubai15;

import com.dd.trieu.product1.Product;

import java.util.List;

public class Bai15Stream {
    public static Boolean isHaveProductInCategory(List<Product> productList, int categoryId) {
        Boolean checkCategoryId = null;
        Product products = productList.stream().filter(product -> categoryId == product.getCategoryid()).findAny().orElse(null);
        if (products != null) {
            checkCategoryId = true;
        } else {
            checkCategoryId = false;
        }


        return checkCategoryId;
    }
}
