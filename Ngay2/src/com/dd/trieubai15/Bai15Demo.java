package com.dd.trieubai15;

import com.dd.trieu.product1.Product;

import java.text.ParseException;
import java.util.List;

public class Bai15Demo {
    public static void main(String[] args) throws ParseException {
        List<Product> productList = Product.getPRoduct();
        System.out.println("dùng Stream");
        Boolean product = Bai15Stream.isHaveProductInCategory(productList , 1);
        System.out.println(product);
        System.out.println("Không dùng stream");
        Boolean product1 = Bai15.isHaveProductInCategory(productList,1);
        System.out.println(product1);
    }

}
