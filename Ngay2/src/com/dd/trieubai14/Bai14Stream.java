package com.dd.trieubai14;

import com.dd.trieu.product1.Product;

import java.util.List;
import java.util.stream.Collectors;

public class Bai14Stream {
    public static int totalProduct(List<Product> productList, boolean isDelete) {

        List<Product> products = productList.stream().filter(x -> x.isDelete() == false).collect(Collectors.toList());
        int total = products.stream().reduce(0, (a, b) -> a + b.getQulity(), Integer::sum);

        return total ;
    }

}
