package com.dd.trieubai14;

import com.dd.trieu.product1.Product;

import java.text.ParseException;
import java.util.List;

public class Bai14Demo {
    public static void main(String[] args) throws ParseException {
        List<Product> productList = Product.getPRoduct();

        System.out.println("dùng Stream");
        int product = Bai14Stream.totalProduct(productList, false);
        System.out.println(product);
        System.out.println(" không dùng Stream");
        int product1 = Bai14.totalProduct(productList);
        System.out.println(product);

    }
}
