package com.dd.trieubai14;

import com.dd.trieu.product1.Product;

import java.util.List;

public class Bai14 {
    public static Integer totalProduct(List<Product> listProducts) {
        int total = 0;
        for (Product product : listProducts) {
            if (product.isDelete() == false) {
                total += product.getQulity();
            }
        }
        return total;
    }
}
