package com.dd.trieubai16;

import com.dd.trieu.product1.Product;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Bai16Stream {
    public static List<String> filterProductBySaleDate(List<Product> productList, int id) {

        List<String> products = productList.stream().
                filter(product -> product.getQulity() > 0
                        && product.getSaleDate().
                        isAfter(LocalDate.now())).
                map(product -> product.getName()).
                collect(Collectors.toList());


        return products;
    }
}
