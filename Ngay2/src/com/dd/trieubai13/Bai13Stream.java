package com.dd.trieubai13;

import  com.dd.trieu.product1.Product;

import java.time.LocalDate;

import java.util.List;
import java.util.stream.Collectors;

public class Bai13Stream {
    public static List<Product> filterProductByQuility(List<Product> productList, boolean isDelete) {

        List<Product> products2 = productList.stream().filter(product -> product.isDelete() == false && product.getSaleDate().isBefore(LocalDate.now())).collect(Collectors.toList());
        System.out.println(products2);
        return products2;
    }
}
