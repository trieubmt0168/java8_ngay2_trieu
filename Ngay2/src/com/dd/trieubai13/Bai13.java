package com.dd.trieubai13;

import com.dd.trieu.product1.Product;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Bai13 {
    public static List<Product> filterProductByQuility(List<Product> productList, boolean isDelete) {
        List<Product> products = new ArrayList<Product>();
        for (Product product : productList) {
            if (product.getSaleDate().isBefore(LocalDate.now()) && product.isDelete() == false) {
                products.add(product);
            }

        }
        return products;
    }


}
