package com.dd.trieu.product1;

import java.util.List;

public class StreamBai11 {
    public static Product filterProductById(List<Product> listProducts, int id) {
        Product products = listProducts.stream().filter(product -> id == product.getId()).findAny().orElse(null);
        return products;
    }

}
