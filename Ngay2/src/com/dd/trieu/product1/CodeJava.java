package com.dd.trieu.product1;

import java.util.List;

public class CodeJava {
    public static Product filterProductByid(List<Product> productEntityList, Integer id){
        Product product = null;
        for (Product temp : productEntityList) {
            if (id == temp.getId()) {
                product = temp;
            }
        }
        return product;

    }

}
