package com.dd.trieu.product1;

import java.text.ParseException;
import java.util.List;

public class Bai11Demo {
    public static void main(String[] args) throws ParseException {
        List<Product> productList = Product.getPRoduct();

        System.out.println("Lấy id theo thên bằng Stream");
        Product product = StreamBai11.filterProductById(productList, 1);
        System.out.println("Stream : " + product.getId() + " " + product.getName());

        System.out.println("Lấy id theo thên bằng for");
        Product product1 = Bai11.filterProductByid(productList, 1);
        System.out.println("Stream : " + product1.getId() + " " + product1.getName());




    }
}
